require('dotenv').config()
const express = require('express')
const browserSync = require('browser-sync')
const axios = require('axios')
const fs  = require('fs')

const app = express()

browserSync({
    proxy: 'localhost:3000',
    open: false,
})

browserSync.watch('*.md').on('change', () => {
    browserSync.notify("Reloading")
    browserSync.reload()
})

app.use( async (req, resp, next) => {

    // TODO: Solve workaround removing trailing slash when look for file
    const markdown = '.' + req.url.replace(/\/$/, '') + '.md'

    if (!fs.existsSync(markdown)) {
        console.log(req.url + ' [static]')
        next(); return
    }

    browserSync.notify("AAAAAAAAAAAA")
    try {
        const content  = await fs.promises.readFile(markdown)
        const { data } = await axios.post(process.env.GITLAB_MARKDOWN_API, {
            text: content.toString(),
            gfm: true,
            project: process.env.GITLAB_PROJECT
        })

        console.log(req.url + ' [Ok]')
        resp.set('Content-Type', 'text/html')
        resp.send(`
            <link rel="stylesheet" href="/styles.css">
            <style>
                body { font-size: 1rem; padding: 30px; }
                img { width: 100%; }
            </style>
            <body>
                ${ data.html }
                <script>
                document.querySelectorAll('img').forEach( img => {
                    img.src = '/' + img.getAttribute('data-src')
                })
                </script>
            </body>`
        )

    } catch (err) {
        console.log(req.url + ' [Err]')
        resp.send(err)
    }
})
app.use(express.static('.'))
app.listen(3000)
